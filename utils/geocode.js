const request = require('request')

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ encodeURIComponent(address) +
    '.json?access_token=pk.eyJ1IjoiZ21hamRvdWIiLCJhIjoiY2s0MncyeWFjMDFrczNqbG42aGt6anpneSJ9.fHuQIhaGLJxzaRq6O9cvQQ&limit=1'
    request({ url, json: true }, (error, {body}) => {
        err = undefined, foundLoc=undefined
        if(error) {
            err = 'Unable to connect to location service!'
        } else {
            if(body.features.length === 0){
                err = 'Unable to find location: Try again with different search terms!'
            } else {
                const {center:{0:lon, 1:lat}, place_name:loc} = body.features[0]
                foundLoc = { lat, lon, loc }
            }
        }
        callback(err, foundLoc)
    })
}

module.exports = geocode