const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')
const chalk = require('chalk')

const location = process.argv[2]
if (location) {
    geocode(location, (error, data) => {
        if (data) {
            const {lat, lon, loc} = data
            forecast(lat, lon, (error, fcData) => {
                if (error) {
                    return console.log(error)
                }
                console.log(chalk.inverse(loc))
                console.log(fcData)
            })
        }
        else {
            console.log(chalk.red.inverse('ERROR: ' + error))
        }
    })
} else { console.log(chalk.red.inverse('Please provide a location/address ...')) }